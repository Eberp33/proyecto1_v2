import Juegos
juego = Juegos.Juegos()

opcion = 0
while opcion != 8:
    print("------------Menu de juegos------------")
    print("1. Insertar un juego")
    print("2. Ver lista de juegos")
    print("3. Borrar un juego")
    print("4. Buscar un juego")
    print("5. Actualizar nombre de un juego")
    print("6. Actualizar genero de un juego")
    print("7. Actualizar precio de un juego")
    print("8 . Salir")
    opcion = int(input("Digite una opción: "))

    if opcion == 1:
        print("Insertar un juego")
        nombre = input("Nombre del juego: ").title()
        genero = input("Genero del juego: ").title()
        precio = float(input("Precio del juego: "))
        juego.insertar_juego(nombre, genero, precio)
    elif opcion == 2:
        print("Ver lista de juegos")
        juego.ver_juegos()
    elif opcion == 3:
        print("Borrar un juego")
        juego.borrar_juego()
    elif opcion == 4:
        print("Buscar un juego")
        juego.buscar_juego()
    elif opcion == 5:
        print("Actualizar nombre de un juego")
        juego.actualizar_nombre_juego()
    elif opcion == 6:
        print("Actualizar genero de un juego")
        juego.actualizar_genero_juego()
    elif opcion == 7:
        print("Actualizar precio de un juego")
        juego.actualizar_precio_juego()
    elif opcion == 8:
        break
    else:
        print("Opción incorrecta, digite de nuevo")

juego.conexion.close()
