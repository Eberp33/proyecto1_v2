import psycopg2


class Juegos:
    def __init__(self):
        self.conexion = psycopg2.connect(
            database="Juegos",
            user="postgres",
            host="localhost",
            password="admin",
            port="5432"
        )

    def insertar_juego(self, nombre, genero, precio):
        cursor = self.conexion.cursor()
        cursor.execute(f"INSERT INTO juego VALUES ('{nombre}', '{genero}', '{precio}')")
        self.conexion.commit()
        print("Juego insertado correctamente")

    def ver_juegos(self):
        cursor = self.conexion.cursor()
        cursor.execute("SELECT * FROM juego")
        lista_juegos = cursor.fetchall()
        if len(lista_juegos) > 0:
            for i in lista_juegos:
                print(f"Nombre del juego: {i[0]}")
                print(f"Genero del juego: {i[1]}")
                print(f"Precio del juego: ${i[2]}")
                print("-" * 48)
        else:
            print("La lista está vacía, no se puede mostrar nada")

    def borrar_juego(self):
        cursor = self.conexion.cursor()
        cursor.execute("SELECT * FROM juego")
        cantidad_juegos = cursor.rowcount
        if cantidad_juegos > 0:
            nombre = input("Nombre del juego a borrar: ").title()
            cursor.execute(f"DELETE FROM juego WHERE nombre_juego = '{nombre}'")
            borrados = cursor.rowcount
            self.conexion.commit()
            if borrados == 1:
                print("El juego se borró con éxito")
            else:
                print(f"No se encontró el juego \"{nombre}\" en la lista")
        else:
            print("No se puede borra nada la lista está vacía")

    def buscar_juego(self):
        cursor = self.conexion.cursor()
        cursor.execute("SELECT * FROM juego")
        cantidad_juegos = cursor.rowcount
        if cantidad_juegos > 0:
            nombre = input("Nombre del juego a buscar: ").title()
            cursor.execute(f"SELECT * FROM juego WHERE nombre_juego = '{nombre}'")
            juego = cursor.fetchone()
            encontrar = cursor.rowcount
            if encontrar == 1:
                print(f"Nombre del juego: {juego[0]}")
                print(f"Genero del juego: {juego[1]}")
                print(f"Precio del juego: {juego[2]}")
            else:
                print("No existe un juego con ese nombre")
        else:
            print("La lista está vacía no se puede buscar nada")

    def actualizar_nombre_juego(self):
        cursor = self.conexion.cursor()
        cursor.execute("SELECT * FROM juego")
        cantidad_juegos = cursor.rowcount
        if cantidad_juegos > 0:
            nombre = input("Nombre del juego que desea actualizar: ").title()
            cursor.execute(f"SELECT * FROM juego WHERE nombre_juego = '{nombre}'")
            encontrar = cursor.rowcount
            if encontrar == 1:
                nombre_actualizado = input("Inserte el nuevo nombre: ").title()
                cursor.execute(f"UPDATE juego SET nombre_juego = '{nombre_actualizado}'WHERE nombre_juego = '{nombre}'")
                self.conexion.commit()
                print("Nombre actualizado correctamente")
            else:
                print("El juego no está en la lista")
        else:
            print("No hay nada que actualizar, la lista está vacía")

    def actualizar_genero_juego(self):
        cursor = self.conexion.cursor()
        cursor.execute("SELECT * FROM juego")
        cantidad_juegos = cursor.rowcount
        if cantidad_juegos > 0:
            nombre = input("Nombre del juego que desea actualizar: ").title()
            cursor.execute(f"SELECT * FROM juego WHERE nombre_juego = '{nombre}'")
            encontrar = cursor.rowcount
            if encontrar == 1:
                genero_actualizado = input("Inserte el nuevo genero: ").title()
                cursor.execute(f"UPDATE juego SET genero_juego = '{genero_actualizado}'WHERE nombre_juego = '{nombre}'")
                self.conexion.commit()
                print("Genero actualizado correctamente")
            else:
                print("El juego no está en la lista")
        else:
            print("No hay nada que actualizar, la lista está vacía")

    def actualizar_precio_juego(self):
        cursor = self.conexion.cursor()
        cursor.execute("SELECT * FROM juego")
        cantidad_juegos = cursor.rowcount
        if cantidad_juegos > 0:
            nombre = input("Nombre del juego que desea actualizar: ").title()
            cursor.execute(f"SELECT * FROM juego WHERE nombre_juego = '{nombre}'")
            encontrar = cursor.rowcount
            if encontrar == 1:
                precio_actualizado = input("Inserte el nuevo precio: ").title()
                cursor.execute(f"UPDATE juego SET precio_juego = '{precio_actualizado}'WHERE nombre_juego = '{nombre}'")
                self.conexion.commit()
                print("Precio actualizado correctamente")
            else:
                print("El juego no está en la lista")
        else:
            print("No hay nada que actualizar, la lista está vacía")
